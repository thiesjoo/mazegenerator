import pygame, numpy
WHITE = (255,255,255)
GREEN = (0,255,0)
RED = (255,0,0)
GREY = (50,50,50)
DARK_GREY = (100,100,100)
LIGHT_RED = (100,0,0)


class Cell:
    def __init__(self, i, j, cellSizeX,cellSizeY, mazeSize, neighbors=None):
        self.visited = False
        if neighbors is None:
            self.neighbors = [True,True,True,True] #Left, Up, Right, Down
        else:
            self.neighbors = neighbors 
        #True = Wall, False = nothing, None = No neighbor

        #Border checking
        if j == 0:
            self.neighbors[1] = None
        elif j+1 >= mazeSize[1]:
            # self.visited = True
            self.neighbors[3] = None

        if i == 0:
            self.neighbors[0] = None
        elif i+1 >= mazeSize[0]:
            self.neighbors[2] = None 

        self.i = i
        self.j = j
        self.cellSizeX = cellSizeX
        self.cellSizeY = cellSizeY


        self.startPoint = False
        self.endPoint = False
        self.path = False

        self.block = False

        # A*
        self.g = 0
        self.h = 0
        self.parent = None


    def draw(self,screen):
        color = (0,0,0)

        color = GREY if self.visited else color
        color = DARK_GREY if self.path else color
        color = LIGHT_RED if self.path == None else color

        color = WHITE if self.block else color

        color = GREEN if self.startPoint else color
        color = RED if self.endPoint else color

        pygame.draw.rect(screen,color,(self.i*self.cellSizeX, self.j*self.cellSizeY, self.cellSizeX, self.cellSizeY), 0)

        if self.visited:
            for i,value in enumerate(self.neighbors):
                if value or value is None:
                    if i == 0:
                        pygame.draw.line(screen, WHITE, (self.i*self.cellSizeX,self.j*self.cellSizeY), (self.i*self.cellSizeX,(self.j+1)*self.cellSizeY),1)
                    elif i == 1:
                        pygame.draw.line(screen, WHITE, (self.i*self.cellSizeX,self.j*self.cellSizeY), ((self.i+1)*self.cellSizeX,(self.j)*self.cellSizeY),1)
                    elif i == 2:
                        pygame.draw.line(screen, WHITE, ((self.i+1)*self.cellSizeX-1,self.j*self.cellSizeY), ((self.i+1)*self.cellSizeX-1,(self.j+1)*self.cellSizeY),1)
                    elif i == 3:
                        pygame.draw.line(screen, WHITE, (self.i*self.cellSizeX,(self.j+1)*self.cellSizeY-1), ((self.i+1)*self.cellSizeX,(self.j+1)*self.cellSizeY-1),1)

def unvisitedNeighbors(current, maze):
    unvistedNeigh = []
    for i, neighbor in enumerate(current.neighbors):
        if neighbor is not None:
            if i == 0:
                newCell = get(maze, (current.i-1, current.j))
            elif i == 1:
                newCell = get(maze, (current.i, current.j-1))
            elif i == 2:
                newCell = get(maze, (current.i+1, current.j))
            elif i == 3:
                newCell = get(maze, (current.i, current.j+1))
            if not newCell.visited:
                unvistedNeigh.append(i)
    return unvistedNeigh


def unvisited(array):
    unvisited = False
    for i in range(len(array)):
        if unvisited:
            break
        for j in range(len(array[i])):
            if (array[i][j].visited == False):
                unvisited = True
                break
    return unvisited

def children(maze,current, check=False):
    nodes = []
    directions = []
    for choice, neighbor in enumerate(current.neighbors):
        if neighbor == check:
            if choice == 0:
                directions.append((-1,0))
                temp = (current.i-1,current.j)
            elif choice == 1:
                directions.append((0,-1))
                temp = (current.i,current.j-1)
            elif choice == 2:
                temp = (current.i+1,current.j)
                directions.append((+1,0))
            elif choice == 3:
               temp = (current.i,current.j+1)
               directions.append((0,+1))
            nodes.append(maze[temp[0]][temp[1]])
    return (nodes,directions)

def get(maze,location):
    assert location[0] > -1 and location[0] < len(maze), "Outside of borders, i"
    assert location[1] > -1 and location[1] < len(maze[1]), "Outside of borders, j"

    cell = maze[location[0]][location[1]]
    if cell == None:
        print("WHAT IS GOING ON HERE")
    return cell


def exportMaze(maze, screen, filename):
    print("Exporting maze with filename: ", filename)
    filename = "export/"+filename
    toExport = [0]*len(maze)

    for i in range(len(maze)):
        toExport[i] = [0]*len(maze[0])
        for j in range(len(maze[i])):
            cell = maze[i][j]
            neighbors = cell.neighbors
            if cell.startPoint:
                neighbors.append("Start")
            elif cell.endPoint:
                neighbors.append("End")
            toExport[i][j] = neighbors

    numpy.save(filename,toExport)
    pygame.image.save(screen,filename+".png")

def loadMaze(name,w,h, cellSize, mazeSize):
    mazeLoaded = numpy.load(name, allow_pickle=True)
    maze = [0]*(w)
    for i in range(len(maze)):
        maze[i] = [0]*(h)
        for j in range(len(maze[i])):
            lijst = list(mazeLoaded[i][j])
            maze[i][j] = Cell(i,j,cellSize[0],cellSize[1], mazeSize, lijst[:4]) 
            if len(lijst) == 5:
                if lijst[4] == "Start":
                    maze[i][j].startPoint = True
                elif lijst[4] == "End":
                    maze[i][j].endPoint = True

            maze[i][j].visited = True
    print("Loaded the maze")
    return maze
