import pygame, math, sys, argparse

if sys.platform == "win32":
    import ctypes
    ctypes.windll.user32.SetProcessDPIAware() #To stop windows from scaling automatically

from generators import *
from solvers import *
from helper import exportMaze, loadMaze


# --------------------------------- Arguments -------------------------------- #
parser = argparse.ArgumentParser()
#Generating
parser.add_argument("--ownBuild","-b", help="Build your own maze",action="store_true")
parser.add_argument("--recursiveBacktracking", "-rb", help="Generate maze with recursive backtracking",action="store_true")

#Solving
parser.add_argument("--aStar", "-a", help="Solve maze with aStar",action="store_true")
parser.add_argument("--wall", "-w", help="Solve maze with a wall algorithm. Provide a number between 1-4. (See README for which way)",type=int)
parser.add_argument("-BFS", help="Solve maze with a BFS algorithm. Provide 'queue'",type=str)

parser.add_argument("--all", help="Solve maze with all programmed algorithms",action="store_true")

#Misc
parser.add_argument("--file", "-f", help="Load a numpy array. (Without .npy extension)",type=str)


parser.add_argument("--width", help="Width of the maze",type=int)
parser.add_argument("--height", help="Height of the maze",type=int)
parser.add_argument("--iterationsPerFrame","-itf", help="Amount of iterations per framne",type=int)

args = parser.parse_args()

# ------------------------------- Initializing ------------------------------- #
pygame.init()

mazeSize = (200,100)
resolution = 10  #Has great impact on memory usage
displayMovesGenerator = False # has great impact on gpu usage and speed
displayMovesSolver = True 
iterationsPerFrame = 1000 # has impact on the speed of the algorithm


size = (1600, 800) 
fakeSize = (resolution*mazeSize[0], resolution*mazeSize[1])

screen = pygame.display.set_mode(size)
fakeScreen = pygame.Surface(fakeSize)

pygame.display.set_caption("My maze generator")
clock = pygame.time.Clock()     
carryOn = True

cellSize = (round(fakeSize[0]/mazeSize[0]),  round(fakeSize[1]/mazeSize[1]))
print(cellSize)

maze = None
busy = False
frame = None

def changeMazeSize(w,h):
    global maze, mazeSize, fakeScreen, fakeSize, cellSize, generator
    print("Changing maze size to",w,h)
    maze = None
    mazeSize = (w,h)
    fakeSize = (resolution*mazeSize[0], resolution*mazeSize[1])
    fakeScreen = pygame.Surface(fakeSize)
    cellSize = (math.floor(fakeSize[0]/mazeSize[0]),  math.floor(fakeSize[1]/mazeSize[1]))
    generator = Generate(mazeSize,cellSize,display,displayMovesGenerator,iterationsPerFrame)

def display(newMaze, bestPerformance=True):
    global maze, carryOn, busy, frame

    if not newMaze == None:
        maze = newMaze


    for event in pygame.event.get():
        if event.type == pygame.QUIT: 
            carryOn = False
        elif event.type == pygame.KEYUP:
# ----------------------------- All key controls ----------------------------- #
            if event.key == pygame.K_r and not busy:
                busy = True
                print("Started generating")
                generator.generateMaze(generator.recursiveBacktracking)
                args.ownBuild = False
                busy = False
            elif event.key == pygame.K_b and not busy:
                busy = True
                generator.generateMaze(generator.empty)
                args.ownBuild = True
                busy = False
            elif event.key == pygame.K_e and not busy:
                busy = True
                exportMaze(maze,fakeScreen, "maze"+str(mazeSize))
                busy = False
            elif event.key == pygame.K_j and not busy and not maze == None:
                busy = True
                solver.solveMaze(maze,solver.leftWall)
                busy = False
            elif event.key == pygame.K_l and not busy and not maze == None:
                busy = True
                solver.solveMaze(maze,solver.rightWall)
                busy = False
            elif event.key == pygame.K_k and not busy and not maze == None:
                busy = True
                solver.solveMaze(maze,solver.downWall)
                busy = False
            elif event.key == pygame.K_i and not busy and not maze == None:
                busy = True
                solver.solveMaze(maze,solver.upWall)
                busy = False
            elif event.key == pygame.K_y and not busy and not maze == None:
                busy = True
                solver.solveMaze(maze,solver.aStar)
                busy = False
            elif event.key == pygame.K_h and not busy and not maze == None:
                busy = True
                solver.solveMaze(maze,solver.BFS)
                busy = False
            elif event.key == pygame.K_a and not busy and not maze == None:
                busy = True
                allSolvers = [solver.solveMaze(maze,solver.aStar, False),solver.solveMaze(maze,solver.BFS), solver.solveMaze(maze,solver.leftWall, False),solver.solveMaze(maze,solver.rightWall, False), solver.solveMaze(maze,solver.upWall, False),solver.solveMaze(maze,solver.downWall, False)]
                fastest = min(allSolvers, key = lambda t: t[1])
                print(fastest[0]+ " was the fastest algorithm with a time of ", round(fastest[1],2), " seconds")
                busy = False
            elif event.key == pygame.K_ESCAPE or event.key == pygame.K_q:
                carryOn = False
        elif event.type == pygame.MOUSEMOTION and args.ownBuild:
            x,y = event.pos
            pressed = pygame.mouse.get_pressed()

            factorX = fakeSize[0]/size[0]
            factorY = fakeSize[1]/size[1]
            newx, newy = (int((x*factorX)/cellSize[0]), int((y*factorY)/cellSize[1]))
            cell = maze[newx][newy]
            if pressed[0]:
                if not cell.block:
                    cell.block = True
                    for i in range(len(cell.neighbors)):
                        if cell.neighbors[i] is not None:
                            cell.neighbors[i] = True
            elif pressed[1]:
                if cell.block:
                    cell.block = False
                    for i in range(len(cell.neighbors)):
                        if cell.neighbors[i] is True:
                            cell.neighbors[i] = False
            display(None)

    if bestPerformance:
        if maze is not None:
            for i in range(len(maze)):
                for j in range(len(maze[i])):
                    maze[i][j].draw(fakeScreen)
        screen.fill((0,0,255))

        frame = pygame.transform.scale(fakeScreen, size)
        screen.blit(frame, (frame.get_rect()))

    pygame.display.flip()
    clock.tick()

generator = Generate(mazeSize,cellSize,display,displayMovesGenerator,iterationsPerFrame)
solver = Solver(display,displayMovesSolver,iterationsPerFrame)

# ----------------------------- Argument parsing ----------------------------- #
if len(sys.argv) > 1:

    if args.file:
        print("Trying to load a maze from a file")
        print(args.file)
        if args.file.startswith("maze("):
            w = int(args.file[5:args.file.index(',')])
            h = int(args.file[args.file.index(',')+1:len(args.file)-1])
            changeMazeSize(w,h)
            display(loadMaze(args.file+".npy", w, h, cellSize, mazeSize))
            print("Blitted screen once. Continuing now")
    else:
        if args.width is not None and args.height is not None:
            changeMazeSize(args.width,args.height)

        if args.recursiveBacktracking:
            print("Started")
            generator.generateMaze(generator.recursiveBacktracking)
        elif args.ownBuild:
            print("Building on your own")
            generator.generateMaze(generator.empty)
        else:
            generator.generateMaze(generator.recursiveBacktracking)

    if args.aStar:
        solver.solveMaze(maze,solver.aStar)
    elif args.wall:
        if args.wall == 1:
            solver.solveMaze(maze,solver.upWall)
        elif args.wall == 2:
            solver.solveMaze(maze,solver.leftWall)
        elif args.wall == 3:
            solver.solveMaze(maze,solver.downWall)
        elif args.wall == 4:
            solver.solveMaze(maze,solver.rightWall)
        else:
            print("This is not a correct wall input")
    elif args.BFS:
        if args.BFS == "queue":
            print("Started BFS solver")
            solver.solveMaze(maze,solver.BFS)
        else:
            print("This is not a correct BFS input")
    elif args.all:
        allSolvers = [solver.solveMaze(maze,solver.aStar, False),solver.solveMaze(maze,solver.BFS), solver.solveMaze(maze,solver.leftWall, False),solver.solveMaze(maze,solver.rightWall, False), solver.solveMaze(maze,solver.upWall, False),solver.solveMaze(maze,solver.downWall, False)]
        fastest = min(allSolvers, key = lambda t: t[1])
        print(fastest[0]+ " was the fastest algorithm with a time of ", round(fastest[1],2), " seconds")

while carryOn:
    display(None, False)
 
pygame.quit()
exit()