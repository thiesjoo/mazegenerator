import time, random, math
from helper import get, children, unvisitedNeighbors

class Solver():
    def __init__(self,display, displayMoves,iterationsPerFrame):
        self.display = display
        self.displayMoves = displayMoves
        self.iterationsPerFrame = iterationsPerFrame

    def solveMaze(self,maze,algorithm, log=True):
        # This function prepares the maze for the algorithm and calls the algorithm
        startPoint = None
        endPoint = None

        for i in range(len(maze)):
            for j in range(len(maze[0])):
                maze[i][j].path = False
                if maze[i][j].startPoint:
                    startPoint = (i,j)
                elif maze[i][j].endPoint:
                    endPoint = (i,j)
        assert startPoint, "No start found"
        assert endPoint, "No endpoint found"

        startTime = time.time()

        current = startPoint  
        maze,iterations = algorithm(maze, startPoint, endPoint)

        endTime = time.time()
        totalTime = endTime - startTime

        for i in range(len(maze)):
            for j in range(len(maze[0])):
                if maze[i][j].path == None:
                    maze[i][j].path = False

        self.display(maze)
        print("Finished solving with "+ algorithm.__name__ +". Solved a "+str(len(maze))+ "x"+str(len(maze[1])) +" maze in", round(totalTime,2), "seconds with",iterations,"iterations")
        return (algorithm.__name__,totalTime)

    def aStar(self,maze,start,end):
        iterations = 0
        current = maze[start[0]][start[1]]
        open_list = []
        closed_list = []

        open_list.append(current)

        while len(open_list) > 0:
            current = open_list[0]
            if current == get(maze,end):
                while current != get(maze,start):
                    current.path = True
                    current = current.parent
                return (maze,iterations)

            open_list.remove(current)
            closed_list.append(current)
            current.path = None

            for node in children(maze,current)[0]:
                if node in closed_list:
                    continue
                
                if node in open_list:
                    new_g = current.g + 10
                    if node.g > new_g:
                        node.g = new_g
                        node.parent = current
                else:
                    node.g = current.g +10
                    node.h = 10 * (abs(node.i - end[0]) + abs(node.j - end[1]))
                    node.parent = current
                    open_list.append(node)

            iterations += 1
            if self.displayMoves and iterations % self.iterationsPerFrame == 0:
                self.display(maze)

        print("No path found")
        return (maze,iterations)

    def BFS(self,maze,start,end):
        iterations = 0
        current = maze[start[0]][start[1]]
        queue = []

        queue.append(current)
        while len(queue) > 0:
            current = queue.pop(random.randrange(len(queue)))
            if current == get(maze,end):
                while current != get(maze,start):
                    current.path = True
                    current = current.parent
                return (maze,iterations)
        
            for node in children(maze,current)[0]:
                if node.path == False:
                    node.path = None
                    node.parent = current
                    queue.append(node)
            
            iterations += 1
            if self.displayMoves and iterations % self.iterationsPerFrame == 0:
                self.display(maze)

        print("No path found")
        return (maze,iterations)


    def leftWall(self,maze,start,end):
        return self.wallSolver(0,maze,start,end)

    def rightWall(self,maze,start,end):
        return self.wallSolver(2,maze,start,end)

    def downWall(self,maze,start,end):
        return self.wallSolver(3,maze,start,end)

    def upWall(self,maze,start,end):
        return self.wallSolver(1,maze,start,end)

    def wallSolver(self,wall,maze,start,end, ):
        iterations = 0
        current = start

        wallEncoding = [(-1,0), (0,-1), (1,0), (0,1)]

        moves = []
        while not current == end:
            iterations += 1
            unvistedNeigh = []
            temp = children(maze,get(maze,current))
            for i,node in enumerate(temp[0]):
                if node.path == False:
                    unvistedNeigh.append(temp[1][i])

            get(maze,current).path = True
            if len(unvistedNeigh) > 0:
                while not wallEncoding[wall] in unvistedNeigh:
                    if wall == 0:
                        wall = 3
                    else:
                        wall -= 1

                moves.append(wallEncoding[wall])
                current = (current[0]+wallEncoding[wall][0],current[1]+wallEncoding[wall][1])
              
            else:
                get(maze,current).path = None
                if len(moves) > 0:
                    currentMove = moves.pop()
                    current = (current[0]+(-currentMove[0]), current[1]+(-currentMove[1]))
                else:
                    print("No solution found. You sure this is an innerconnected maze?")
                    return (maze,iterations)

            if self.displayMoves and iterations % self.iterationsPerFrame == 0:
                self.display(maze)
        return (maze,iterations)


