# Maze generator
## By Thies Nieborg


Start the program with `python index.py`
See the args with `python index.py -h`

All maze generators are stored in `generators.py` and all maze solvers are stored in `solvers.py`

To load a file use the `-f` flag. After that specify the name of the file without the extension. It has to be located within the directoy where `index.py` is stored.

### Controls
#### Generator
R: Generate a maze with *recursive backtracking*

#### Solver
I,J,K,L: Solve current maze with *up wall*, *left wall*, *down wall*, *right wall* algorithm

J: Solve current maze with *A** algorithm

A: Solve maze with all algorithms and compare the time


#### Misc
E: Export current maze



#### Todo:
Visualisation of A* and BFS algorithm doesn't work correctly (Places red squares everywhere)