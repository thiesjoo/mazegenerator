import time, random, math
from helper import get, unvisited, Cell, unvisitedNeighbors

class Generate():
    def __init__(self,mazeSize, cellSize,display, displayMoves,iterationsPerFrame):
        self.mazeSize = mazeSize
        self.cellSize = cellSize
        self.display = display
        self.displayMoves = displayMoves
        self.iterationsPerFrame = iterationsPerFrame

    def generateMaze(self,algorithm, log=True):
        maze = [0]*(self.mazeSize[0])
        for i in range(len(maze)):
            maze[i] = [0]*(self.mazeSize[1])
            for j in range(len(maze[i])):
                maze[i][j] = Cell(i,j,self.cellSize[0],self.cellSize[1], self.mazeSize)


        current = (random.randint(0,len(maze)-1),random.randint(0,len(maze[0])-1) )
        start = current
        get(maze,current).visited = True
        get(maze,current).startPoint = True

        startTime = time.time()
        maze,iterations = algorithm(maze, current)
        endTime = time.time()  
        print("Finished! Now getting endpoint")     

        #End point
        distance = 0
        while distance < self.mazeSize[0]/2 or current == start: #Distance must be a minimum half of the field
            current = (random.randint(0,len(maze)-1),random.randint(0,len(maze[0])-1) )
            distance = math.hypot(current[0]-start[0],current[1]-start[1])

        if log:
            print("Begin location: ",start)
            print("End location: ",current, " with distance of ",round(distance))
        get(maze,current).endPoint = True

        self.display(maze)
        totalTime = endTime - startTime

        print("Finished generating with "+ algorithm.__name__ +". Generated a "+str(self.mazeSize[0])+ "x"+str(self.mazeSize[1]) +" maze in", round(totalTime,2), "seconds with",iterations,"iterations")

    def recursiveBacktracking(self,maze,current):
        stack = []
        iterations = 0
        currentCell = get(maze,current)
        stack.append(currentCell)

        while len(stack) > 0:
            currentCell = stack.pop(random.randrange(len(stack)))
            current = (currentCell.i, currentCell.j)
            nodes = unvisitedNeighbors(currentCell,maze)
            if len(nodes) > 0:
                stack.append(currentCell)
                choice = random.choice(nodes)
                currentCell.neighbors[choice] = False
                if choice == 0:
                    current = (current[0]-1,current[1])
                elif choice == 1:
                    current = (current[0],current[1]-1)
                elif choice == 2:
                    current = (current[0]+1,current[1])
                elif choice == 3:
                    current = (current[0],current[1]+1)
                currentCell = get(maze,current)
                currentCell.neighbors[(choice+2)%4] = False
                currentCell.visited = True
                stack.append(currentCell)

            iterations += 1
            if self.displayMoves and iterations % self.iterationsPerFrame == 0:
                self.display(maze)
        return (maze, iterations)
                    
   
    def empty(self, maze, current):
        for i in range(len(maze)):
            for j in range(len(maze[i])):
                maze[i][j] = Cell(i,j,self.cellSize[0],self.cellSize[1], self.mazeSize, [False]*4)
                maze[i][j].visited = True
                if (i == current[0] and j == current[1]):
                    maze[i][j].startPoint = True
        return(maze,1)